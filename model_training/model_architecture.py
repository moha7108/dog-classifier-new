import torchvision.models as models
import torch.nn as nn
import torch.nn.functional as F

# define the CNN architecture

def model_arch():

    model_transfer = models.vgg16(pretrained=True)
    for param in model_transfer.features.parameters():
        param.requires_grad = False


    n_inputs = model_transfer.classifier[6].in_features

    last_layer = nn.Linear(n_inputs, 133)

    model_transfer.classifier[6] = last_layer

    return model_transfer
